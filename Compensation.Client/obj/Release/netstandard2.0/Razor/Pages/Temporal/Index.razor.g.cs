#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "71e2e4b38541f4cda21bdc63c157b1fe0e9e1dac"
// <auto-generated/>
#pragma warning disable 1591
namespace Compensation.Client.Pages.Temporal
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
using Compensation.Client.Data;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MainLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/indice-temporal")]
    public class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>Consulta Temporal de Comisiones</h3>\r\n<br>\r\n<br>\r\n\r\n\r\n");
#line 13 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
 if (oficinaList == null)
{

#line default
#line hidden
            __builder.AddContent(1, "    ");
            __builder.AddMarkupContent(2, "<p><em>Loading...</em></p>\r\n");
#line 16 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
}
else
{


#line default
#line hidden
            __builder.AddContent(3, "    ");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "row");
            __builder.AddAttribute(6, "style", "padding-top:10px");
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.OpenElement(8, "div");
            __builder.AddAttribute(9, "class", "form-group col-md-2");
            __builder.AddMarkupContent(10, "\r\n\r\n            ");
            __builder.OpenElement(11, "select");
            __builder.AddAttribute(12, "class", "custom-select");
            __builder.AddAttribute(13, "id", "oficinaDrop");
            __builder.AddAttribute(14, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 23 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                                                                       OficinaSelectionChanged

#line default
#line hidden
            ));
            __builder.AddMarkupContent(15, "\r\n                ");
            __builder.OpenElement(16, "option");
            __builder.AddAttribute(17, "value", true);
            __builder.AddContent(18, "-- Select Oficina --");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\r\n                ");
            __builder.OpenElement(20, "option");
            __builder.AddAttribute(21, "value", "0");
            __builder.AddContent(22, "-- Todo --");
            __builder.CloseElement();
            __builder.AddMarkupContent(23, "\r\n");
#line 26 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                 if (oficinaList != null)
                {
                    

#line default
#line hidden
#line 28 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                     foreach (var itemOficina in oficinaList)
                    {

#line default
#line hidden
            __builder.AddContent(24, "                        ");
            __builder.OpenElement(25, "option");
            __builder.AddAttribute(26, "value", 
#line 30 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                                        itemOficina.CodOficina

#line default
#line hidden
            );
            __builder.AddContent(27, 
#line 30 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                                                                 itemOficina.NombreOficina

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(28, "\r\n");
#line 31 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                    }

#line default
#line hidden
#line 31 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                     
                }

#line default
#line hidden
            __builder.AddMarkupContent(29, "\r\n\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(30, "\r\n\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n\r\n\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(32, "\r\n");
#line 66 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"




}

#line default
#line hidden
            __builder.AddMarkupContent(33, "\r\n\r\n\r\n\r\n");
#line 75 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
 if (@ResumenView != null)
{

#line default
#line hidden
            __builder.AddContent(34, "    ");
            __builder.OpenElement(35, "h4");
            __builder.AddContent(36, 
#line 77 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
         periodo

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(37, "\r\n");
#line 78 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"

    
}

#line default
#line hidden
            __builder.AddMarkupContent(38, "\r\n\r\n");
#line 83 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
 if (resumen != null)
{

#line default
#line hidden
            __builder.OpenElement(39, "table");
            __builder.AddAttribute(40, "class", "table table-striped");
            __builder.AddMarkupContent(41, "\r\n    ");
            __builder.AddMarkupContent(42, "<thead class=\"thead-dark\">\r\n        <tr>\r\n            <th></th>\r\n            <th>Ficha</th>\r\n            <th>Consultor</th>\r\n            <th align=\"center\">Monto</th>\r\n\r\n        </tr>\r\n    </thead>\r\n    ");
            __builder.OpenElement(43, "tbody");
            __builder.AddMarkupContent(44, "\r\n");
#line 96 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
         if (pCResumenOficinaTemporal != null)
        {
            

#line default
#line hidden
#line 98 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
             foreach (var item in pCResumenOficinaTemporal)
            {

#line default
#line hidden
            __builder.AddContent(45, "                ");
            __builder.OpenElement(46, "tr");
            __builder.AddMarkupContent(47, "\r\n\r\n\r\n                    ");
            __builder.OpenElement(48, "td");
            __builder.AddContent(49, 
#line 103 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                         item.NombreOficina

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(50, "\r\n\r\n\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(51, "\r\n");
#line 107 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                 foreach (var itemResumen in resumen.Where(r => r.CodigoOficina == item.CodigoOficina))
                {

#line default
#line hidden
            __builder.AddContent(52, "                    ");
            __builder.OpenElement(53, "tr");
            __builder.AddMarkupContent(54, "\r\n                        ");
            __builder.OpenElement(55, "td");
            __builder.AddMarkupContent(56, "\r\n                            ");
            __builder.OpenElement(57, "a");
            __builder.AddAttribute(58, "class", "btn btn-success");
            __builder.AddAttribute(59, "href", "indice-temporal-detalle/" + (
#line 111 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                                                                                      itemResumen.Id

#line default
#line hidden
            ));
            __builder.AddContent(60, "Ver Detalle");
            __builder.CloseElement();
            __builder.AddMarkupContent(61, "\r\n\r\n                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(62, "\r\n\r\n                        ");
            __builder.OpenElement(63, "td");
            __builder.AddContent(64, 
#line 115 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                             itemResumen.Ficha

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(65, "\r\n                        ");
            __builder.OpenElement(66, "td");
            __builder.AddContent(67, 
#line 116 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                             itemResumen.NombreVendedor

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(68, "\r\n                        ");
            __builder.OpenElement(69, "td");
            __builder.AddAttribute(70, "align", "right");
            __builder.AddContent(71, 
#line 117 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                                           itemResumen.MontoString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(72, "\r\n\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(73, "\r\n");
#line 120 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"

                }

#line default
#line hidden
            __builder.AddContent(74, "                ");
            __builder.OpenElement(75, "tr");
            __builder.AddMarkupContent(76, "\r\n\r\n\r\n                    <td></td>\r\n                    ");
            __builder.AddMarkupContent(77, "<td class=\"font-weight-bold\">Total Oficina==========></td>\r\n                    <td></td>\r\n                    ");
            __builder.OpenElement(78, "td");
            __builder.AddAttribute(79, "align", "right");
            __builder.AddAttribute(80, "class", "font-weight-bold");
            __builder.AddContent(81, 
#line 128 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                                                                item.MontoString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(82, "\r\n\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(83, "\r\n");
#line 131 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"

            }

#line default
#line hidden
#line 132 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
             
        }

#line default
#line hidden
            __builder.AddContent(84, "        ");
            __builder.OpenElement(85, "tr");
            __builder.AddMarkupContent(86, "\r\n\r\n\r\n            <td></td>\r\n            ");
            __builder.AddMarkupContent(87, "<td class=\"font-weight-bold\">Total Periodo==========></td>\r\n            <td></td>\r\n            ");
            __builder.OpenElement(88, "td");
            __builder.AddAttribute(89, "align", "right");
            __builder.AddAttribute(90, "class", "font-weight-bold");
            __builder.AddContent(91, 
#line 140 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
                                                        TotalperiodoString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(92, "\r\n\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(93, "\r\n\r\n\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(94, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(95, "\r\n");
#line 147 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
#line 148 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Temporal\Index.razor"
       
    PCOficina pCOficina = new PCOficina();

    PCOficina[] oficinaList { get; set; }

    PCVendedor[] vendedorList { get; set; }

    PCResumenComisionTemporal[] resumen { get; set; }

    PCResumenOficinaTemporal[] pCResumenOficinaTemporal { get; set; }




    PCResumenComisionTemporal ResumenView { get; set; }

    public string periodo = "";
    public int oficinaId = 0;
    string oficinaName = "";
    string NombreOficina = "";
    string vendedorName = "";
    decimal TotalOficina = 0;
    string TotalOficinaString = "";

    decimal Totalperiodo = 0;
    string TotalperiodoString = "";

    protected override async Task OnInitializedAsync()
    {
        await GetDataOficina();
        await GetDataResumenOficina();
    }

    async Task GetDataOficina()
    {
        oficinaList = await oficinaService.GetOficinaAsync();
    }

    async Task GetDataResumenOficina()
    {
        pCResumenOficinaTemporal = await resumenOficinaTemporalService.GetResumenOficinaTemporalAsync();
    }


    async Task OficinaSelectionChanged(ChangeEventArgs e)
    {

        if (int.TryParse(e.Value.ToString(), out int id))
        {
            oficinaId = id;
            await GetData();
            vendedorList = await vendedorService.GetVendedorAsync(id);
        }



    }

    async Task VendedorSelectionChanged(ChangeEventArgs e)
    {
        string id = e.Value.ToString();

        vendedorName = id;

    }

    async Task GetData()
    {
       // resumen = await temporalService.GetResumenTemporalByIdAsync(oficinaId);
       await GetDataResumenOficina();
        resumen = await temporalService.getTemporalAsync();
        if (oficinaId!=0)
        {
            resumen = resumen.Where(r => r.CodigoOficina == oficinaId).ToArray();
        }
        resumen = resumen.OrderBy(r => r.CodigoOficina).ToArray();
        if (resumen != null)
        {
            ResumenView = resumen.FirstOrDefault();
            if (ResumenView != null)
            {
                periodo = ResumenView.Periodo;
                NombreOficina = ResumenView.NombreOficina;
            }
            TotalOficina = 0;
            foreach (var item in resumen)
            {
                TotalOficina = TotalOficina + item.Monto;
            }
            TotalOficinaString = ToCurrencyString(TotalOficina);

        }

        if (oficinaId!=0)
        {
            pCResumenOficinaTemporal = pCResumenOficinaTemporal.Where(r => r.CodigoOficina == oficinaId).ToArray();
        }

            Totalperiodo = 0;
            foreach (var item in pCResumenOficinaTemporal)
            {
                Totalperiodo = Totalperiodo + item.Monto;
            }
            TotalperiodoString = ToCurrencyString(Totalperiodo);


    }


    public string ToCurrencyString(decimal d)
    {


        return String.Format("{0:0,0.0}", d);
    }



#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ResumenOficinaTemporalService resumenOficinaTemporalService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private TemporalService temporalService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private VendedorService vendedorService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private OficinaService oficinaService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
