#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3efa11d165467527222c33359d03e3aaaab6af71"
// <auto-generated/>
#pragma warning disable 1591
namespace Compensation.Client.Pages.Historico
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
using Compensation.Client.Data;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MainLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/indice-historicoOrden")]
    public class IndexOrden : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>Historico Orden</h3>\r\n\r\n\r\n\r\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "card");
            __builder.AddAttribute(3, "style", "width: 18rem;");
            __builder.AddMarkupContent(4, "\r\n    \r\n    ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "card-body");
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.AddMarkupContent(8, "<h5 class=\"card-title\">Orden</h5>\r\n        ");
            __builder.OpenElement(9, "div");
            __builder.AddMarkupContent(10, "\r\n\r\n            ");
            __builder.OpenElement(11, "input");
            __builder.AddAttribute(12, "type", "text");
            __builder.AddAttribute(13, "class", "form-control");
            __builder.AddAttribute(14, "aria-label", "Sizing example input");
            __builder.AddAttribute(15, "aria-describedby", "inputGroup-sizing-lg");
            __builder.AddAttribute(16, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#line 16 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                                      orden

#line default
#line hidden
            ));
            __builder.AddAttribute(17, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => orden = __value, orden));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(18, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\r\n        <br>\r\n        ");
            __builder.OpenElement(20, "div");
            __builder.AddMarkupContent(21, "\r\n            ");
            __builder.OpenElement(22, "button");
            __builder.AddAttribute(23, "class", "btn btn-primary");
            __builder.AddAttribute(24, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 20 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                                                        () => GetDataHistorico(orden)

#line default
#line hidden
            ));
            __builder.AddContent(25, " Buscar");
            __builder.CloseElement();
            __builder.AddMarkupContent(26, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(27, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(28, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(29, "\r\n<br>\r\n\r\n");
#line 26 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
 if (historico != null)
{

#line default
#line hidden
            __builder.AddContent(30, "    ");
            __builder.OpenElement(31, "table");
            __builder.AddAttribute(32, "class", "table table-striped");
            __builder.AddMarkupContent(33, "\r\n        ");
            __builder.AddMarkupContent(34, @"<thead class=""thead-dark"">
        
            <tr>

                <th>Periodo desde</th>
                <th>Periodo hasta</th>
                <th>Tr</th>
                <th>Doc.</th>
                <th>Orden</th>
                <th>Producto</th>
                <th>Monto</th>
                <th>Tipo Pago</th>
                <th>Comision</th>
                <th data-toggle=""tooltip"" data-placement=""top"" title=""Porcentaje Comision Flat"">% Tabla 1</th>
                <th data-toggle=""tooltip"" data-placement=""top"" title=""Porc Rango Cumplimiento Cuota General"">% Tabla 2</th>
                <th data-toggle=""tooltip"" data-placement=""top"" title=""Porc Cantidad Cuotas Cumplidas"">% Tabla 3</th>
                <th>Cant. Cuotas</th>

            </tr>
        </thead>
        ");
            __builder.OpenElement(35, "tbody");
            __builder.AddMarkupContent(36, "\r\n");
#line 50 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
             foreach (var item in historico)
            {

#line default
#line hidden
            __builder.AddContent(37, "            ");
            __builder.OpenElement(38, "tr");
            __builder.AddMarkupContent(39, "\r\n                ");
            __builder.OpenElement(40, "td");
            __builder.AddContent(41, 
#line 53 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.PeriodoDesde

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(42, "\r\n                ");
            __builder.OpenElement(43, "td");
            __builder.AddContent(44, 
#line 54 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.PeriodoHasta

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(45, "\r\n                ");
            __builder.OpenElement(46, "td");
            __builder.AddContent(47, 
#line 55 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.Transaccion

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(48, "\r\n                ");
            __builder.OpenElement(49, "td");
            __builder.AddContent(50, 
#line 56 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.DocumentoString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(51, "\r\n                ");
            __builder.OpenElement(52, "td");
            __builder.AddContent(53, 
#line 57 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.OrdenString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(54, "\r\n                ");
            __builder.OpenElement(55, "td");
            __builder.AddContent(56, 
#line 58 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.Producto

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(57, "\r\n                ");
            __builder.OpenElement(58, "td");
            __builder.AddAttribute(59, "align", "right");
            __builder.AddContent(60, 
#line 59 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                                   item.MontoRealString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(61, "\r\n                ");
            __builder.OpenElement(62, "td");
            __builder.AddContent(63, 
#line 60 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.DescripcionTipoPago

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(64, "\r\n                ");
            __builder.OpenElement(65, "td");
            __builder.AddAttribute(66, "align", "right");
            __builder.AddContent(67, 
#line 61 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                                   item.MontoString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(68, "\r\n                ");
            __builder.OpenElement(69, "td");
            __builder.AddContent(70, 
#line 62 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.PorcFlat

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(71, "\r\n                ");
            __builder.OpenElement(72, "td");
            __builder.AddContent(73, 
#line 63 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.PorcRangoCumplimientoCuotaGeneral

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(74, "\r\n                ");
            __builder.OpenElement(75, "td");
            __builder.AddContent(76, 
#line 64 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.PorcCantidadCuotasCumplidas

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(77, "\r\n                ");
            __builder.OpenElement(78, "td");
            __builder.AddContent(79, 
#line 65 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                     item.CantidadCuotasCumplidas

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(80, "\r\n\r\n\r\n\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(81, "\r\n");
#line 70 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
            }

#line default
#line hidden
            __builder.AddContent(82, "            ");
            __builder.OpenElement(83, "tr");
            __builder.AddMarkupContent(84, "\r\n\r\n\r\n                <td></td>\r\n                ");
            __builder.AddMarkupContent(85, "<td class=\"font-weight-bold\">Total Vendedor==========></td>\r\n                <td></td>\r\n                <td></td>\r\n                <td></td>\r\n                <td></td>\r\n                ");
            __builder.OpenElement(86, "td");
            __builder.AddAttribute(87, "align", "right");
            __builder.AddAttribute(88, "class", "font-weight-bold");
            __builder.AddContent(89, 
#line 80 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
                                                            TotalVendedorString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(90, "\r\n                <td></td>\r\n                <td></td>\r\n                <td></td>\r\n                <td></td>\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(91, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(92, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(93, "\r\n");
#line 88 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
#line 91 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\Historico\IndexOrden.razor"
       

    PCHistorico[] historicoView { get; set; }
    PCHistorico[] historico { get; set; }


    [CascadingParameter]
    private Task<AuthenticationState> authenticationState { get; set; }

    string Rol = "";
    string usuarioConectado = "";


    string orden = "";

    decimal TotalVendedor = 0;
    string TotalVendedorString = "";


    async Task GetDataHistorico(string id)
    {

          var authState = await authenticationState;
        var usuario = authState.User;

        var claims = usuario.Claims.ToList();
        if (claims != null)
        {
            foreach (var item in claims)
            {
                if (item.Type == "Rol")
                {
                    Rol = item.Value;

                }
                if (item.Type == "unique_name")
                {

                    usuarioConectado = item.Value;
                }

            }
        }



        historico = await historicoService.GetHistoricoByIOrdenAsync(id);
          if (Rol=="394")
        {
            historico = historico.Where(h => h.IdVendedor == usuarioConectado).ToArray();
        }
        historico = historico.OrderBy(h => h.FechaRegistro).ToArray();

        TotalVendedor = 0;
        if (historico!=null)
        {
            foreach (var item in historico)
            {
                TotalVendedor = TotalVendedor + item.BsComision;
            }
            TotalVendedorString = ToCurrencyString(TotalVendedor);
        }





    }

    public string ToCurrencyString(decimal d)
    {


        return String.Format("{0:0,0.0}", d);
    }



#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HistoricoService historicoService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
