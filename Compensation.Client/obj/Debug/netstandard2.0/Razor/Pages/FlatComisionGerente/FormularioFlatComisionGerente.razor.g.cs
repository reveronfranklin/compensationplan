#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "45b333b0aee50fa8a72cacb5e3b3fc69b8c5c698"
// <auto-generated/>
#pragma warning disable 1591
namespace Compensation.Client.Pages.FlatComisionGerente
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MainLayout))]
    public class FormularioFlatComisionGerente : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(0);
            __builder.AddAttribute(1, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#line 1 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                  flatComisionGerente

#line default
#line hidden
            ));
            __builder.AddAttribute(2, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#line 1 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                       OnValidSubmit

#line default
#line hidden
            )));
            __builder.AddAttribute(3, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(4, "\r\n    ");
                __builder2.OpenElement(5, "div");
                __builder2.AddAttribute(6, "class", "form-field");
                __builder2.AddMarkupContent(7, "\r\n        ");
                __builder2.AddMarkupContent(8, "<label>Tipo Producto:</label>\r\n        ");
                __builder2.OpenElement(9, "div");
                __builder2.AddMarkupContent(10, "\r\n            ");
                __builder2.OpenElement(11, "select");
                __builder2.AddAttribute(12, "class", "custom-select");
                __builder2.AddAttribute(13, "id", "flatDrop");
                __builder2.AddAttribute(14, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                                    FlatSelectionChanged

#line default
#line hidden
                ));
                __builder2.AddAttribute(15, "value", 
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                                                                  SelectedValue

#line default
#line hidden
                );
                __builder2.AddMarkupContent(16, "\r\n                ");
                __builder2.OpenElement(17, "option");
                __builder2.AddAttribute(18, "value", true);
                __builder2.AddContent(19, "-- Select Tipo Flat --");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(20, "\r\n");
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                 if (flatComision != null)
                {
                    

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                     foreach (var item in flatComision)
                    {

#line default
#line hidden
                __builder2.AddContent(21, "                        ");
                __builder2.OpenElement(22, "option");
                __builder2.AddAttribute(23, "value", 
#line 11 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                        item.Id

#line default
#line hidden
                );
                __builder2.AddContent(24, 
#line 11 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                  item.Descripcion

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(25, "\r\n");
#line 12 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                    }

#line default
#line hidden
#line 12 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                     
                }

#line default
#line hidden
                __builder2.AddMarkupContent(26, "\r\n\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(27, "\r\n\r\n        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(28, "\r\n    ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(29, "\r\n    <br>\r\n    ");
                __builder2.OpenElement(30, "div");
                __builder2.AddAttribute(31, "class", "form-field");
                __builder2.AddMarkupContent(32, "\r\n        ");
                __builder2.AddMarkupContent(33, "<label>Gerente:</label>\r\n        ");
                __builder2.OpenElement(34, "select");
                __builder2.AddAttribute(35, "class", "custom-select");
                __builder2.AddAttribute(36, "id", "vendedorDrop");
                __builder2.AddAttribute(37, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 23 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                                    VendedorSelectionChanged

#line default
#line hidden
                ));
                __builder2.AddAttribute(38, "value", 
#line 23 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                                                                      SelectedValueGerente

#line default
#line hidden
                );
                __builder2.AddMarkupContent(39, "\r\n            ");
                __builder2.OpenElement(40, "option");
                __builder2.AddAttribute(41, "value", true);
                __builder2.AddContent(42, "-- Select Vendedor --");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(43, "\r\n");
#line 25 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
             if (vendedorList != null)
            {
                

#line default
#line hidden
#line 27 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                 foreach (var itemVendedor in vendedorList)
                {

#line default
#line hidden
                __builder2.AddContent(44, "                    ");
                __builder2.OpenElement(45, "option");
                __builder2.AddAttribute(46, "value", 
#line 29 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                    itemVendedor.IdVendedor

#line default
#line hidden
                );
                __builder2.AddContent(47, 
#line 29 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                              itemVendedor.Nombre

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(48, "\r\n");
#line 30 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                }

#line default
#line hidden
#line 30 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                 
            }

#line default
#line hidden
                __builder2.AddMarkupContent(49, "\r\n        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(50, "\r\n    ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(51, "\r\n    <br>\r\n    ");
                __builder2.OpenElement(52, "div");
                __builder2.AddAttribute(53, "class", "form-field");
                __builder2.AddMarkupContent(54, "\r\n        ");
                __builder2.AddMarkupContent(55, "<label>Porcentaje Gerente:</label>\r\n        ");
                __builder2.OpenElement(56, "div");
                __builder2.AddMarkupContent(57, "\r\n           \r\n            ");
                __Blazor.Compensation.Client.Pages.FlatComisionGerente.FormularioFlatComisionGerente.TypeInference.CreateInputNumber_0(__builder2, 58, 59, "form-control", 60, 
#line 40 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                            flatComisionGerente.Porcentaje

#line default
#line hidden
                , 61, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => flatComisionGerente.Porcentaje = __value, flatComisionGerente.Porcentaje)), 62, () => flatComisionGerente.Porcentaje);
                __builder2.AddMarkupContent(63, "\r\n            ");
                __Blazor.Compensation.Client.Pages.FlatComisionGerente.FormularioFlatComisionGerente.TypeInference.CreateValidationMessage_1(__builder2, 64, 65, 
#line 41 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                      ()=> flatComisionGerente.Porcentaje

#line default
#line hidden
                );
                __builder2.AddMarkupContent(66, "\r\n        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(67, "\r\n    ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(68, "\r\n    <br>\r\n\r\n    ");
                __builder2.OpenElement(69, "button");
                __builder2.AddAttribute(70, "type", "submit");
                __builder2.AddAttribute(71, "class", "btn btn-success");
                __builder2.AddContent(72, 
#line 46 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
                                                   TextoBoton

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(73, "\r\n    ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(74);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(75, "\r\n");
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#line 50 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\FlatComisionGerente\FormularioFlatComisionGerente.razor"
       

    [Parameter] public PCFlatComision[] flatComision { get; set; }

    [Parameter] public PCVendedor[] vendedorList { get; set; }

    [Parameter] public PCFlatComisionGerente flatComisionGerente { get; set; } = new PCFlatComisionGerente();

    [Parameter] public string TextoBoton { get; set; } = "Salvar";

    [Parameter] public EventCallback OnValidSubmit { get; set; }

    public PCFlatComision flatComisionSelected { get; set; } = new PCFlatComision();

    public PCVendedor vendedorSelected { get; set; } = new PCVendedor();



    int SelectedValue = 0;
    string SelectedValueGerente = "";

    protected async override Task OnParametersSetAsync()
    {
        SelectedValue =  flatComisionGerente.IdFlatComision;
        SelectedValueGerente =  flatComisionGerente.Gerente;

    }

    async Task FlatSelectionChanged(ChangeEventArgs e)
    {

        if (int.TryParse(e.Value.ToString(), out int id))
        {
            flatComisionGerente.IdFlatComision = id;
            SelectedValue = id;
            flatComisionSelected = flatComision.Where(f => f.Id == id).FirstOrDefault();
            flatComisionGerente.DescripcionCategoria = flatComisionSelected.Descripcion;
        }

    }


    async Task VendedorSelectionChanged(ChangeEventArgs e)
    {
        string id = e.Value.ToString();

        flatComisionGerente.Gerente = id;
        vendedorSelected = vendedorList.Where(v => v.IdVendedor == flatComisionGerente.Gerente).FirstOrDefault();
        flatComisionGerente.NombreGerente = vendedorSelected.Nombre;


    }




#line default
#line hidden
    }
}
namespace __Blazor.Compensation.Client.Pages.FlatComisionGerente.FormularioFlatComisionGerente
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateInputNumber_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, TValue __arg1, int __seq2, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg2, int __seq3, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg3)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Class", __arg0);
        __builder.AddAttribute(__seq1, "Value", __arg1);
        __builder.AddAttribute(__seq2, "ValueChanged", __arg2);
        __builder.AddAttribute(__seq3, "ValueExpression", __arg3);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
