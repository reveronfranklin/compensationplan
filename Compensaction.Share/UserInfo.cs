﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Compensaction.Share
{
    public class UserInfo
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

        public string NombreUsuario { get; set; }

        public string Rol { get; set; }

        public bool Validado { get; set; }
    }
}
