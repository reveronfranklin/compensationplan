#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "64b1c069c5b5a65902e03d0f580b38258e68d17d"
// <auto-generated/>
#pragma warning disable 1591
namespace Compensation.Client.Pages.ResumenComisionHistorico
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
using Compensation.Client.Data;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MainLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/indice-ResumenComisionHistorico")]
    public class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>Resumen Comisión Histórico</h3>\r\n<br>\r\n\r\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "card");
            __builder.AddAttribute(3, "style", "width: 58rem;");
            __builder.AddMarkupContent(4, "\r\n\r\n    ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "card-body");
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.AddMarkupContent(8, "<h5 class=\"card-title\">Filtros</h5>\r\n\r\n");
#line 22 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
         if (periodoList == null)
        {

#line default
#line hidden
            __builder.AddContent(9, "            ");
            __builder.AddMarkupContent(10, "<p><em>Loading...</em></p>\r\n");
#line 25 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
        }
        else
        {

#line default
#line hidden
            __builder.AddContent(11, "            ");
            __builder.OpenElement(12, "div");
            __builder.AddAttribute(13, "class", "row");
            __builder.AddMarkupContent(14, "\r\n                ");
            __builder.OpenElement(15, "div");
            __builder.AddAttribute(16, "class", "col-sm-4");
            __builder.AddMarkupContent(17, "\r\n                    ");
            __builder.OpenElement(18, "div");
            __builder.AddAttribute(19, "class", "card");
            __builder.AddMarkupContent(20, "\r\n                        ");
            __builder.OpenElement(21, "div");
            __builder.AddAttribute(22, "class", "card-body");
            __builder.AddMarkupContent(23, "\r\n                            ");
            __builder.AddMarkupContent(24, "<h5 class=\"card-title\">Periodo</h5>\r\n                            <p class=\"card-text\"></p>\r\n                            ");
            __builder.OpenElement(25, "select");
            __builder.AddAttribute(26, "class", "custom-select");
            __builder.AddAttribute(27, "id", "periodoDrop");
            __builder.AddAttribute(28, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 34 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                                                       PeriodoSelectionChanged

#line default
#line hidden
            ));
            __builder.AddMarkupContent(29, "\r\n                                ");
            __builder.OpenElement(30, "option");
            __builder.AddAttribute(31, "value", true);
            __builder.AddContent(32, "-- Select Periodo --");
            __builder.CloseElement();
            __builder.AddMarkupContent(33, "\r\n");
#line 36 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                 if (periodoList != null)
                                {
                                    

#line default
#line hidden
#line 38 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                     foreach (var itemOficina in periodoList)
                                    {

#line default
#line hidden
            __builder.AddContent(34, "                                        ");
            __builder.OpenElement(35, "option");
            __builder.AddAttribute(36, "value", 
#line 40 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                        itemOficina.ID

#line default
#line hidden
            );
            __builder.AddContent(37, 
#line 40 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                                         itemOficina.FechaDescripcion

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(38, "\r\n");
#line 41 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                    }

#line default
#line hidden
#line 41 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                     
                                }

#line default
#line hidden
            __builder.AddMarkupContent(39, "\r\n\r\n                            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(40, "\r\n                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(41, "\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(42, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(43, "\r\n");
#line 49 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                 if (Rol == "393")
                {

#line default
#line hidden
            __builder.AddContent(44, "                    ");
            __builder.OpenElement(45, "div");
            __builder.AddAttribute(46, "class", "col-sm-4");
            __builder.AddMarkupContent(47, "\r\n                        ");
            __builder.OpenElement(48, "div");
            __builder.AddAttribute(49, "class", "card");
            __builder.AddMarkupContent(50, "\r\n                            ");
            __builder.OpenElement(51, "div");
            __builder.AddAttribute(52, "class", "card-body");
            __builder.AddMarkupContent(53, "\r\n                                ");
            __builder.AddMarkupContent(54, "<h5 class=\"card-title\">Oficina</h5>\r\n                                <p class=\"card-text\"></p>\r\n                                ");
            __builder.OpenElement(55, "select");
            __builder.AddAttribute(56, "class", "custom-select");
            __builder.AddAttribute(57, "id", "oficinaDrop");
            __builder.AddAttribute(58, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 56 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                                                           OficinaSelectionChanged

#line default
#line hidden
            ));
            __builder.AddMarkupContent(59, "\r\n                                    ");
            __builder.OpenElement(60, "option");
            __builder.AddAttribute(61, "value", true);
            __builder.AddContent(62, "-- Select Oficina --");
            __builder.CloseElement();
            __builder.AddMarkupContent(63, "\r\n");
#line 58 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                     if (oficinaList != null)
                                    {
                                        

#line default
#line hidden
#line 60 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                         foreach (var itemOficina in oficinaList)
                                        {

#line default
#line hidden
            __builder.AddContent(64, "                                            ");
            __builder.OpenElement(65, "option");
            __builder.AddAttribute(66, "value", 
#line 62 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                            itemOficina.CodOficina

#line default
#line hidden
            );
            __builder.AddContent(67, 
#line 62 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                                                     itemOficina.NombreOficina

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(68, "\r\n");
#line 63 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                        }

#line default
#line hidden
#line 63 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                         
                                    }

#line default
#line hidden
            __builder.AddMarkupContent(69, "\r\n\r\n                                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(70, "\r\n                            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(71, "\r\n                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(72, "\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(73, "\r\n");
#line 71 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                }

#line default
#line hidden
            __builder.AddMarkupContent(74, "\r\n\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(75, "\r\n");
#line 75 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"

        }

#line default
#line hidden
            __builder.AddMarkupContent(76, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(77, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(78, "\r\n\r\n\r\n");
#line 82 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
 if (historicoView == null)
{

#line default
#line hidden
            __builder.AddContent(79, "    ");
            __builder.AddMarkupContent(80, "<tesxt>  </tesxt>\r\n");
#line 85 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
}
else if (historicoView.Length == 0)
{

#line default
#line hidden
            __builder.AddContent(81, "    ");
            __builder.AddMarkupContent(82, "<tesxt>  No existen datos en histórico....</tesxt>\r\n");
#line 89 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
}
else
{

#line default
#line hidden
            __builder.AddContent(83, "    ");
            __builder.OpenElement(84, "div");
            __builder.AddAttribute(85, "class", "row");
            __builder.AddMarkupContent(86, "\r\n        ");
            __builder.OpenElement(87, "div");
            __builder.AddAttribute(88, "class", "col-md-12");
            __builder.AddMarkupContent(89, "\r\n            ");
            __builder.OpenElement(90, "div");
            __builder.AddAttribute(91, "class", "panel panel-default");
            __builder.AddMarkupContent(92, "\r\n                ");
            __builder.AddMarkupContent(93, "<div class=\"panel-heading\">\r\n                    <h3 class=\"panel-title\">Mantenimiento de Periodo</h3>\r\n                </div>\r\n                ");
            __builder.OpenElement(94, "div");
            __builder.AddAttribute(95, "class", "panel-body");
            __builder.AddMarkupContent(96, "\r\n                    ");
            __builder.OpenElement(97, "table");
            __builder.AddAttribute(98, "class", "display");
            __builder.AddAttribute(99, "style", "width:100%");
            __builder.AddAttribute(100, "id", "example");
            __builder.AddMarkupContent(101, "\r\n                        ");
            __builder.AddMarkupContent(102, @"<thead>

                            <tr>
                                <th>Código</th>
                                <th>Nombre Vendedor</th>
                                <th>Ficha</th>
                                <th>Monto</th>
                                <th></th>
                            </tr>
                        </thead>
                        ");
            __builder.OpenElement(103, "tbody");
            __builder.AddMarkupContent(104, "\r\n");
#line 111 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                             foreach (var item in historicoView)
                            {

#line default
#line hidden
            __builder.AddContent(105, "                                ");
            __builder.OpenElement(106, "tr");
            __builder.AddMarkupContent(107, "\r\n                                    ");
            __builder.OpenElement(108, "td");
            __builder.AddContent(109, 
#line 114 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                         item.CodigoVendedor

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(110, "\r\n                                    ");
            __builder.OpenElement(111, "td");
            __builder.AddContent(112, 
#line 115 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                         item.NombreVendedor

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(113, "\r\n                                    ");
            __builder.OpenElement(114, "td");
            __builder.AddContent(115, 
#line 116 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                         item.Ficha

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(116, "\r\n                                    ");
            __builder.OpenElement(117, "td");
            __builder.AddContent(118, 
#line 117 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                         item.MontoString

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(119, "\r\n                                    ");
            __builder.OpenElement(120, "td");
            __builder.AddMarkupContent(121, "\r\n                                        ");
            __builder.OpenElement(122, "a");
            __builder.AddAttribute(123, "class", "btn btn-success");
            __builder.AddAttribute(124, "href", "/indice-DetalleComisionHistorico/" + (
#line 119 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                                                                           item.IdPeriodo

#line default
#line hidden
            ) + "/" + (
#line 119 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                                                                                                                           item.CodigoVendedor

#line default
#line hidden
            ));
            __builder.AddContent(125, "Ver Detalle");
            __builder.CloseElement();
            __builder.AddMarkupContent(126, "\r\n                                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(127, "\r\n                                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(128, "\r\n");
#line 122 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
                            }

#line default
#line hidden
            __builder.AddContent(129, "                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(130, "\r\n\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(131, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(132, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(133, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(134, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(135, "\r\n");
#line 130 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
#line 132 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\ResumenComisionHistorico\Index.razor"
       

    int IdPeriodo = 0;
    int IdOficina = 0;
    string Rol = "";
    string usuarioConectado = "";

    WSMY686[] periodoList { get; set; }
    WSMY686[] periodoView { get; set; }

    PCOficina pCOficina = new PCOficina();
    PCOficina[] oficinaList { get; set; }

    PCResumenComisionHistorico[] historicoView { get; set; }
    PCResumenComisionHistorico[] historico { get; set; }

    [CascadingParameter]
    private Task<AuthenticationState> authenticationState { get; set; }

    protected override async Task OnInitializedAsync()
    {

        var authState = await authenticationState;
        var usuario = authState.User;

        var claims = usuario.Claims.ToList();
        if (claims != null)
        {
            foreach (var item in claims)
            {

                if (item.Type == "Rol")
                {
                    Rol = item.Value;

                }
                if (item.Type == "unique_name")
                {

                    usuarioConectado = item.Value;
                }

            }
        }
        await GetDataPeriodo();
        await GetDataOficina();

    }

    async Task GetDataPeriodo()
    {
        periodoView = await periodoService.getPeriodoAsync();

       
        periodoList = periodoView.OrderByDescending(p => p.Hasta).ToArray();
    }

    async Task GetDataOficina()
    {
        oficinaList = await oficinaService.GetOficinaAsync();
    }

    async Task PeriodoSelectionChanged(ChangeEventArgs e)
    {

        if (int.TryParse(e.Value.ToString(), out int id))
        {
            IdPeriodo = id;

            historicoView = null;
            if (Rol == "394")
            {
                await GetDataResumenComisionHistorico();
                await JS.InvokeAsync<object>("TestDataTablesAdd", "#example");
            }

        }

    }

    async Task OficinaSelectionChanged(ChangeEventArgs e)
    {
        if (int.TryParse(e.Value.ToString(), out int id))
        {
            IdOficina = id;

            historicoView = null;

            await GetDataResumenComisionHistorico();
            await JS.InvokeAsync<object>("TestDataTablesAdd", "#example");
        }
    }


    async Task GetDataResumenComisionHistorico()
    {

        //Busca resumen comision historico por IdPeriodo
        historico = await resumenComisionHistoricoService.ResumenComisionHistoricoAsync(IdPeriodo);

        //Filtro por Oficina



        if (Rol == "394")
        {
            Console.WriteLine("Usuario Conectad:  " + usuarioConectado);
            historico = historico.Where(item => item.CodigoVendedor == usuarioConectado).ToArray();
        }
        else
        {
            historico = historico.Where(item => item.CodigoOficina == IdOficina).ToArray();
        }

        //Vista filtrada
        historicoView = historico;

    }


#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager UriHelper { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ResumenComisionHistoricoService resumenComisionHistoricoService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JS { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private OficinaService oficinaService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private PeriodoService periodoService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
