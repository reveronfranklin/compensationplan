﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Compensaction.Share
{
    public partial class SegRol
    {
        public SegRol()
        {

            this.SegUsuarioRol = new HashSet<SegUsuarioRol>();
        }
        [Key]
        public int IdRol { get; set; }
        public string NombreRol { get; set; }
        public int IdPrograma { get; set; }
        public string DescripcionRol { get; set; }
        public string AbreviadoRol { get; set; }


        public virtual ICollection<SegUsuarioRol> SegUsuarioRol { get; set; }
    }
}
