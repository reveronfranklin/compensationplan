﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Compensaction.Share
{
    public class PCASCII
    {
        [Key]
        public int Id { get; set; }
        public string Ficha { get; set; }
        public string Clave { get; set; }
        public string Codigo { get; set; }
        public decimal NetoPagado { get; set; }
        public string Desde { get; set; }
        public string Hasta { get; set; }
    }
}
