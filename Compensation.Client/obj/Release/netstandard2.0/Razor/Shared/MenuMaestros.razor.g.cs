#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Shared\MenuMaestros.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6a87e3e9646f90a52f7bcdd7c03e90ade302dc24"
// <auto-generated/>
#pragma warning disable 1591
namespace Compensation.Client.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 11 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
    public class MenuMaestros : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "select");
            __builder.AddAttribute(1, "class", "custom-select custom-select-lg mb-3");
            __builder.AddAttribute(2, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Shared\MenuMaestros.razor"
                                                                    MenuSelectionChanged

#line default
#line hidden
            ));
            __builder.AddMarkupContent(3, "\r\n        ");
            __builder.OpenElement(4, "option");
            __builder.AddAttribute(5, "selected", true);
            __builder.AddContent(6, "Maestros");
            __builder.CloseElement();
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.OpenElement(8, "option");
            __builder.AddAttribute(9, "value", "1");
            __builder.AddContent(10, "Periodo");
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n        ");
            __builder.OpenElement(12, "option");
            __builder.AddAttribute(13, "value", "2");
            __builder.AddContent(14, "% Flat Vendedor");
            __builder.CloseElement();
            __builder.AddMarkupContent(15, "\r\n        ");
            __builder.OpenElement(16, "option");
            __builder.AddAttribute(17, "value", "3");
            __builder.AddContent(18, "% Flat Gerente");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\r\n        ");
            __builder.OpenElement(20, "option");
            __builder.AddAttribute(21, "value", "4");
            __builder.AddContent(22, "Cuota General");
            __builder.CloseElement();
            __builder.AddMarkupContent(23, "\r\n        ");
            __builder.OpenElement(24, "option");
            __builder.AddAttribute(25, "value", "5");
            __builder.AddContent(26, "Categorias Cubiertas");
            __builder.CloseElement();
            __builder.AddMarkupContent(27, "\r\n        ");
            __builder.OpenElement(28, "option");
            __builder.AddAttribute(29, "value", "6");
            __builder.AddContent(30, "Tasa");
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n        ");
            __builder.OpenElement(32, "option");
            __builder.AddAttribute(33, "value", "7");
            __builder.AddContent(34, "Producto Cuota");
            __builder.CloseElement();
            __builder.AddMarkupContent(35, "\r\n        ");
            __builder.OpenElement(36, "option");
            __builder.AddAttribute(37, "value", "8");
            __builder.AddContent(38, "Cuota Ventas");
            __builder.CloseElement();
            __builder.AddMarkupContent(39, "\r\n        ");
            __builder.OpenElement(40, "option");
            __builder.AddAttribute(41, "value", "9");
            __builder.AddContent(42, "Ordenes ignoradas");
            __builder.CloseElement();
            __builder.AddMarkupContent(43, "\r\n    ");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#line 15 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Shared\MenuMaestros.razor"
      


    async Task MenuSelectionChanged(ChangeEventArgs e)
    {
        string id = e.Value.ToString();
        if (id == "1")
        {
            UriHelper.NavigateTo("indice-periodo");
        }
          if (id == "2")
        {
            UriHelper.NavigateTo("indice-FlatComision");
        }
        if (id == "3")
        {
            UriHelper.NavigateTo("indice-FlatComisionGerente");
        }
         if (id == "4")
        {
            UriHelper.NavigateTo("indice-RangoCumplimientoCuotaGeneral");
        }
        if (id == "5")
        {
            UriHelper.NavigateTo("indice-PorcCantidadCategoriasCubiertas");
        }
        if (id == "6")
        {
            UriHelper.NavigateTo("indice-tasa-año-mes");
        }
        if (id == "7")
        {
            UriHelper.NavigateTo("indice-ProductoCuota");
        }
         if (id == "8")
        {
            UriHelper.NavigateTo("indice-CuotaVentas");
        }
          if (id == "9")
        {
            UriHelper.NavigateTo("indice-OrdenesPignoradas");
        }
        
    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager UriHelper { get; set; }
    }
}
#pragma warning restore 1591
