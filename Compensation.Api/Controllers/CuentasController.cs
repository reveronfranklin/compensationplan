﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Compensaction.Share;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Compensation.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CuentasController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly CompensationDbContext _context;
        
        public CuentasController(IConfiguration configuration, CompensationDbContext contex)
        {
            _configuration = configuration;
            _context = contex;
        }
       
        [HttpPost("Login")]
        public async Task<ActionResult<UserToken>> Login([FromBody] UserInfo userInfo)
        {
            UserInfo userInfoValidado = new UserInfo();
            userInfoValidado = UsuarioValido(userInfo.Email, userInfo.Password);
            if (userInfoValidado.Validado)
            {

                return BuildToken(userInfoValidado);
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return BadRequest(ModelState);
            }
        }

        private UserToken BuildToken(UserInfo userInfo)
        {
            var claims = new[]
            {
        new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.Email),
        new Claim(ClaimTypes.Name, userInfo.Email),
        new Claim("NombreUsuario", userInfo.NombreUsuario),
        new Claim("Rol", userInfo.Rol),
        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
    };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // Tiempo de expiración del token. En nuestro caso lo hacemos de una hora.
            var expiration = DateTime.UtcNow.AddHours(1);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: null,
               audience: null,
               claims: claims,
               expires: expiration,
               signingCredentials: creds);

            return new UserToken()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }

        private UserInfo UsuarioValido(string Usuario, string Password)
        {

            

            UserInfo userInfo = new UserInfo();
            userInfo.Email = Usuario;
            userInfo.Password = Password;
            userInfo.Validado = false;
            userInfo.NombreUsuario = "";

            var BuscarUsuario = (from u in _context.SegUsuario
                                 join ur in _context.SegUsuarioRol on u.IdUsuario equals ur.IdUsuario
                                 join r in _context.SegRol on ur.IdRol equals r.IdRol
                                 where u.Usuario == Usuario & r.IdPrograma == 147
                                 select new { Clave = u.Clave, IdRol = r.IdRol, NombreUsuario = u.NombreUsuario }).FirstOrDefault();

            if (BuscarUsuario != null)
            {

               
                    if (BuscarUsuario.Clave == ClsUtilitario.ConvertSha1(Password))
                    {

                    userInfo.Validado = true;
                    userInfo.NombreUsuario = BuscarUsuario.NombreUsuario;
                    userInfo.Rol = BuscarUsuario.IdRol.ToString();

                    }
              



            }

            return userInfo;

        }


    }
}