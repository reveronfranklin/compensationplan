﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Compensaction.Share
{
    public partial class SegUsuario
    {
        public SegUsuario()
        {
            this.SegUsuarioRol = new HashSet<SegUsuarioRol>();
        }

        [Key]
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Usuario { get; set; }
        public string Clave { get; set; }
        public bool Inactivo { get; set; }
        public Nullable<System.DateTime> FechaCambio { get; set; }
        public Nullable<int> ConteoExpira { get; set; }
        public Nullable<int> ConteoBloquea { get; set; }
        public string Email { get; set; }
        public string Ficha { get; set; }
        public Nullable<double> An8 { get; set; }
        public string ClasificacionUsuario { get; set; }
        public string Imei1 { get; set; }
        public string EquipoImei1 { get; set; }
        public string Imei2 { get; set; }
        public string EquipoImei2 { get; set; }
        public string Imei3 { get; set; }
        public string EquipoImei3 { get; set; }

        public virtual ICollection<SegUsuarioRol> SegUsuarioRol { get; set; }
    }
}
