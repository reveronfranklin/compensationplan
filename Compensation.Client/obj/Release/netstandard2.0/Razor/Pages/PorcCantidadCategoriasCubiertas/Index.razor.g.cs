#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "575947d91e12aeaeb5d5012aa964120d36fd836f"
// <auto-generated/>
#pragma warning disable 1591
namespace Compensation.Client.Pages.PorcCantidadCategoriasCubiertas
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
using Compensation.Client.Data;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MainLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/indice-PorcCantidadCategoriasCubiertas")]
    public class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>% Cantidad Categorias Cubiertas</h3>\r\n");
            __builder.AddMarkupContent(1, "<div>\r\n    <a class=\"btn btn-success\" href=\"crear-PorcCantidadCategoriasCubiertas\">Nuevo</a>\r\n</div>\r\n");
#line 11 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
 if (pCPorcCantidadCategoriasCubiertas == null)
{

#line default
#line hidden
            __builder.AddContent(2, "    ");
            __builder.AddMarkupContent(3, "<tesxt>  Cargando.....</tesxt>\r\n");
#line 14 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
}
else if (pCPorcCantidadCategoriasCubiertas.Length == 0)
{

#line default
#line hidden
            __builder.AddContent(4, "    ");
            __builder.AddMarkupContent(5, "<tesxt>  No existen datos de % Comision.....</tesxt>\r\n");
#line 18 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
}
else
{

#line default
#line hidden
            __builder.AddContent(6, "    ");
            __builder.OpenElement(7, "table");
            __builder.AddAttribute(8, "class", "table");
            __builder.AddMarkupContent(9, "\r\n        ");
            __builder.AddMarkupContent(10, @"<thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Cantidad</th>
                <th>Porcentaje Vendedor</th>
                <th>Porcentaje Gerente</th>

            </tr>
        </thead>
        ");
            __builder.OpenElement(11, "tbody");
            __builder.AddMarkupContent(12, "\r\n");
#line 33 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
             foreach (var item in pCPorcCantidadCategoriasCubiertas)
            {

#line default
#line hidden
            __builder.AddContent(13, "            ");
            __builder.OpenElement(14, "tr");
            __builder.AddMarkupContent(15, "\r\n                ");
            __builder.OpenElement(16, "td");
            __builder.AddMarkupContent(17, "\r\n                    ");
            __builder.OpenElement(18, "a");
            __builder.AddAttribute(19, "class", "btn btn-success");
            __builder.AddAttribute(20, "href", "editar-PorcCantidadCategoriasCubiertas/" + (
#line 37 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
                                                                                             item.Id

#line default
#line hidden
            ));
            __builder.AddContent(21, "Editar");
            __builder.CloseElement();
            __builder.AddMarkupContent(22, "\r\n                    ");
            __builder.OpenElement(23, "button");
            __builder.AddAttribute(24, "class", "btn btn-danger");
            __builder.AddAttribute(25, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#line 38 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
                                                               () => Borrar(item.Id)

#line default
#line hidden
            ));
            __builder.AddContent(26, "Borrar");
            __builder.CloseElement();
            __builder.AddMarkupContent(27, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(28, "\r\n\r\n                ");
            __builder.OpenElement(29, "td");
            __builder.AddContent(30, 
#line 41 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
                     item.Id

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(31, "\r\n                ");
            __builder.OpenElement(32, "td");
            __builder.AddContent(33, 
#line 42 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
                     item.Cantidad

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(34, "\r\n                ");
            __builder.OpenElement(35, "td");
            __builder.AddContent(36, 
#line 43 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
                     item.Porcentaje

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(37, "\r\n                ");
            __builder.OpenElement(38, "td");
            __builder.AddContent(39, 
#line 44 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
                     item.PorcentajeGerente

#line default
#line hidden
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(40, "\r\n\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(41, "\r\n");
#line 47 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
            }

#line default
#line hidden
            __builder.AddContent(42, "        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(43, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(44, "\r\n");
#line 50 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
#line 52 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\PorcCantidadCategoriasCubiertas\Index.razor"
       

    PCPorcCantidadCategoriasCubiertas[] pCPorcCantidadCategoriasCubiertas { get; set; }

    protected override async Task OnInitializedAsync()
    {
        await GetData();
    }

    async Task GetData()
    {
        pCPorcCantidadCategoriasCubiertas = await porcCantidadCategoriasCubiertasService.GetPorcCantidadCategoriasCubiertasAsync();
    }

    async Task Borrar(int Id)
    {

       
        string Mensaje = $"deseas borrar este Porcentaje?";
        if (await JS.Confirm("Confirmar", Mensaje, Compensation.Client.Helpers.IJSExtensions.TipoMensajeSweetAlert.question))
        {


            await porcCantidadCategoriasCubiertasService.DeletePorcCantidadCategoriasCubiertasAsync(Id);
            await GetData();
        }




    }


#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JS { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private PorcCantidadCategoriasCubiertasService porcCantidadCategoriasCubiertasService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
