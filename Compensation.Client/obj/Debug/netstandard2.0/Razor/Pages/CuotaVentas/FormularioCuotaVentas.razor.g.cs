#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "47e536acdcc6c65e153599ac59b3cad84baea8b2"
// <auto-generated/>
#pragma warning disable 1591
namespace Compensation.Client.Pages.CuotaVentas
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
#line 1 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
using Compensation.Client.Data;

#line default
#line hidden
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MainLayout))]
    public class FormularioCuotaVentas : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(0);
            __builder.AddAttribute(1, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                      cuotaVentas

#line default
#line hidden
            ));
            __builder.AddAttribute(2, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                   OnValidSubmit

#line default
#line hidden
            )));
            __builder.AddAttribute(3, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(4, "\r\n        ");
                __builder2.OpenElement(5, "h4");
                __builder2.AddAttribute(6, "class", "alert-danger");
                __builder2.AddContent(7, 
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                  Mensaje

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(8, "\r\n        ");
                __builder2.OpenElement(9, "div");
                __builder2.AddAttribute(10, "class", "form-field");
                __builder2.AddMarkupContent(11, "\r\n            ");
                __builder2.AddMarkupContent(12, "<label>Año:</label>\r\n            ");
                __builder2.OpenElement(13, "select");
                __builder2.AddAttribute(14, "class", "custom-select");
                __builder2.AddAttribute(15, "id", "tasaDrop");
                __builder2.AddAttribute(16, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                    TasaSelectionChanged

#line default
#line hidden
                ));
                __builder2.AddMarkupContent(17, "\r\n                ");
                __builder2.OpenElement(18, "option");
                __builder2.AddAttribute(19, "value", true);
                __builder2.AddMarkupContent(20, "-- Select Año-Mes--");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(21, "\r\n");
#line 12 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                 if (pCTasaAñoMes != null)
                {
                    

#line default
#line hidden
#line 14 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     foreach (var itemTasa in pCTasaAñoMes)
                    {

#line default
#line hidden
                __builder2.AddContent(22, "                        ");
                __builder2.OpenElement(23, "option");
                __builder2.AddAttribute(24, "value", 
#line 16 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                        itemTasa.Id

#line default
#line hidden
                );
                __builder2.AddContent(25, 
#line 16 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                      itemTasa.Año

#line default
#line hidden
                );
                __builder2.AddContent(26, " - ");
                __builder2.AddContent(27, 
#line 16 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                      itemTasa.Mes

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(28, "\r\n");
#line 17 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                    }

#line default
#line hidden
#line 17 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     
                }

#line default
#line hidden
                __builder2.AddMarkupContent(29, "\r\n\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(30, "\r\n        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(31, "\r\n\r\n       \r\n        ");
                __builder2.OpenElement(32, "div");
                __builder2.AddAttribute(33, "class", "form-field");
                __builder2.AddMarkupContent(34, "\r\n            ");
                __builder2.AddMarkupContent(35, "<label>Año:</label>\r\n            ");
                __builder2.OpenElement(36, "div");
                __builder2.AddMarkupContent(37, "\r\n                ");
                __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas.TypeInference.CreateInputNumber_0(__builder2, 38, 39, "form-control", 40, "true", 41, 
#line 28 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                cuotaVentas.Año

#line default
#line hidden
                , 42, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => cuotaVentas.Año = __value, cuotaVentas.Año)), 43, () => cuotaVentas.Año);
                __builder2.AddMarkupContent(44, "\r\n                ");
                __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas.TypeInference.CreateValidationMessage_1(__builder2, 45, 46, 
#line 29 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                          ()=> cuotaVentas.Año

#line default
#line hidden
                );
                __builder2.AddMarkupContent(47, "\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(48, "\r\n        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(49, "\r\n        <br>\r\n        ");
                __builder2.OpenElement(50, "div");
                __builder2.AddAttribute(51, "class", "form-field");
                __builder2.AddMarkupContent(52, "\r\n            ");
                __builder2.AddMarkupContent(53, "<label>Mes:</label>\r\n            ");
                __builder2.OpenElement(54, "div");
                __builder2.AddMarkupContent(55, "\r\n                ");
                __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas.TypeInference.CreateInputNumber_2(__builder2, 56, 57, "form-control", 58, "true", 59, 
#line 36 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                cuotaVentas.Mes

#line default
#line hidden
                , 60, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => cuotaVentas.Mes = __value, cuotaVentas.Mes)), 61, () => cuotaVentas.Mes);
                __builder2.AddMarkupContent(62, "\r\n                ");
                __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas.TypeInference.CreateValidationMessage_3(__builder2, 63, 64, 
#line 37 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                          ()=> cuotaVentas.Mes

#line default
#line hidden
                );
                __builder2.AddMarkupContent(65, "\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(66, "\r\n        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(67, "\r\n        <br>\r\n");
#line 41 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
         if (@Accion != "Edit")
        {

#line default
#line hidden
                __builder2.AddContent(68, "            ");
                __builder2.OpenElement(69, "select");
                __builder2.AddAttribute(70, "class", "custom-select");
                __builder2.AddAttribute(71, "id", "oficinaDrop");
                __builder2.AddAttribute(72, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 43 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                       OficinaSelectionChanged

#line default
#line hidden
                ));
                __builder2.AddMarkupContent(73, "\r\n                ");
                __builder2.OpenElement(74, "option");
                __builder2.AddAttribute(75, "value", true);
                __builder2.AddContent(76, "-- Select Oficina --");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(77, "\r\n");
#line 45 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                 if (oficinaList != null)
                {
                    

#line default
#line hidden
#line 47 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     foreach (var itemOficina in oficinaList)
                    {

#line default
#line hidden
                __builder2.AddContent(78, "                        ");
                __builder2.OpenElement(79, "option");
                __builder2.AddAttribute(80, "value", 
#line 49 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                        itemOficina.CodOficina

#line default
#line hidden
                );
                __builder2.AddContent(81, 
#line 49 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                 itemOficina.NombreOficina

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(82, "\r\n");
#line 50 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                    }

#line default
#line hidden
#line 50 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     
                }

#line default
#line hidden
                __builder2.AddMarkupContent(83, "\r\n\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(84, "\r\n            <br>\r\n            <br>\r\n            ");
                __builder2.OpenElement(85, "select");
                __builder2.AddAttribute(86, "class", "custom-select");
                __builder2.AddAttribute(87, "id", "vendedorDrop");
                __builder2.AddAttribute(88, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 57 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                        VendedorSelectionChanged

#line default
#line hidden
                ));
                __builder2.AddAttribute(89, "value", 
#line 57 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                                                          codigoVendedor

#line default
#line hidden
                );
                __builder2.AddMarkupContent(90, "\r\n                ");
                __builder2.OpenElement(91, "option");
                __builder2.AddAttribute(92, "value", true);
                __builder2.AddContent(93, "-- Select Vendedor --");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(94, "\r\n");
#line 59 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                 if (vendedorList != null)
                {
                    

#line default
#line hidden
#line 61 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     foreach (var itemVendedor in vendedorList)
                    {

#line default
#line hidden
                __builder2.AddContent(95, "                        ");
                __builder2.OpenElement(96, "option");
                __builder2.AddAttribute(97, "value", 
#line 63 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                        itemVendedor.IdVendedor

#line default
#line hidden
                );
                __builder2.AddContent(98, 
#line 63 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                  itemVendedor.Nombre

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(99, "\r\n");
#line 64 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                    }

#line default
#line hidden
#line 64 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     
                }

#line default
#line hidden
                __builder2.AddMarkupContent(100, "\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(101, "\r\n            <br>\r\n            <br>\r\n");
                __builder2.AddContent(102, "            ");
                __builder2.OpenElement(103, "select");
                __builder2.AddAttribute(104, "class", "custom-select");
                __builder2.AddAttribute(105, "id", "productoDrop");
                __builder2.AddAttribute(106, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#line 71 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                        ProductoSelectionChanged

#line default
#line hidden
                ));
                __builder2.AddAttribute(107, "value", 
#line 71 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                                                          idProductoCuota

#line default
#line hidden
                );
                __builder2.AddMarkupContent(108, "\r\n                ");
                __builder2.OpenElement(109, "option");
                __builder2.AddAttribute(110, "value", true);
                __builder2.AddContent(111, "-- Select Producto --");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(112, "\r\n");
#line 73 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                 if (productoCuota != null)
                {
                    

#line default
#line hidden
#line 75 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     foreach (var itemProducto in productoCuota)
                    {

#line default
#line hidden
                __builder2.AddContent(113, "                        ");
                __builder2.OpenElement(114, "option");
                __builder2.AddAttribute(115, "value", 
#line 77 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                        itemProducto.Id

#line default
#line hidden
                );
                __builder2.AddContent(116, 
#line 77 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                          itemProducto.Descripcion

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(117, "\r\n");
#line 78 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                    }

#line default
#line hidden
#line 78 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                     
                }

#line default
#line hidden
                __builder2.AddMarkupContent(118, "\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(119, "\r\n");
                __builder2.AddMarkupContent(120, "            <br>\r\n            <br>\r\n");
#line 85 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
        }

#line default
#line hidden
                __builder2.AddMarkupContent(121, "\r\n");
#line 87 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
         if (@Accion == "Edit")
        {

#line default
#line hidden
                __builder2.AddContent(122, "            ");
                __builder2.OpenElement(123, "div");
                __builder2.AddAttribute(124, "class", "form-field");
                __builder2.AddMarkupContent(125, "\r\n                ");
                __builder2.AddMarkupContent(126, "<label>Vendedor:</label>\r\n                ");
                __builder2.OpenElement(127, "div");
                __builder2.AddMarkupContent(128, "\r\n                    ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.InputText>(129);
                __builder2.AddAttribute(130, "Class", "form-control");
                __builder2.AddAttribute(131, "disabled", 
#line 92 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                                                         IsDisabled

#line default
#line hidden
                );
                __builder2.AddAttribute(132, "Value", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#line 92 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                  cuotaVentas.NombreVendedor

#line default
#line hidden
                ));
                __builder2.AddAttribute(133, "ValueChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => cuotaVentas.NombreVendedor = __value, cuotaVentas.NombreVendedor))));
                __builder2.AddAttribute(134, "ValueExpression", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Linq.Expressions.Expression<System.Func<System.String>>>(() => cuotaVentas.NombreVendedor));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(135, "\r\n                    ");
                __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas.TypeInference.CreateValidationMessage_4(__builder2, 136, 137, 
#line 93 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                              ()=> cuotaVentas.NombreVendedor

#line default
#line hidden
                );
                __builder2.AddMarkupContent(138, "\r\n                ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(139, "\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(140, "\r\n            <br>\r\n");
#line 97 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
        }

#line default
#line hidden
                __builder2.AddMarkupContent(141, "\r\n\r\n        ");
                __builder2.OpenElement(142, "div");
                __builder2.AddAttribute(143, "class", "form-field");
                __builder2.AddMarkupContent(144, "\r\n            ");
                __builder2.AddMarkupContent(145, "<label>Cuota USD :</label>\r\n            ");
                __builder2.OpenElement(146, "div");
                __builder2.AddMarkupContent(147, "\r\n                ");
                __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas.TypeInference.CreateInputNumber_5(__builder2, 148, 149, "form-control", 150, 
#line 103 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                                cuotaVentas.CuotaUsd

#line default
#line hidden
                , 151, Microsoft.AspNetCore.Components.EventCallback.Factory.Create(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => cuotaVentas.CuotaUsd = __value, cuotaVentas.CuotaUsd)), 152, () => cuotaVentas.CuotaUsd);
                __builder2.AddMarkupContent(153, "\r\n                ");
                __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas.TypeInference.CreateValidationMessage_6(__builder2, 154, 155, 
#line 104 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                          ()=> cuotaVentas.CuotaUsd

#line default
#line hidden
                );
                __builder2.AddMarkupContent(156, "\r\n            ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(157, "\r\n        ");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(158, "\r\n        <br>\r\n\r\n\r\n\r\n        ");
                __builder2.OpenElement(159, "button");
                __builder2.AddAttribute(160, "type", "submit");
                __builder2.AddAttribute(161, "class", "btn btn-success");
                __builder2.AddContent(162, 
#line 111 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
                                                       TextoBoton

#line default
#line hidden
                );
                __builder2.CloseElement();
                __builder2.AddMarkupContent(163, "\r\n        ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(164);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(165, "\r\n    ");
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#line 115 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Pages\CuotaVentas\FormularioCuotaVentas.razor"
       


    [Parameter] public PCCuotaVentas cuotaVentas { get; set; } = new PCCuotaVentas();

    [Parameter] public string TextoBoton { get; set; } = "Salvar";

    [Parameter] public EventCallback OnValidSubmit { get; set; }

    [Parameter] public string Accion { get; set; } = "Edit";

    [Parameter] public string Mensaje { get; set; } = "";

    PCOficina[] oficinaList { get; set; }

    PCVendedor[] vendedorList { get; set; }

    PCProductoCuota[] productoCuota { get; set; }

    PCTasaAñoMes[] pCTasaAñoMes { get; set; }

    PCTasaAñoMes  tasaSelected { get; set; }

    public int oficinaId = 0;
    public int idProductoCuota = 0;
    string codigoVendedor = "";
    bool IsDisabled = true;
    protected async override Task OnParametersSetAsync()
    {
        await GetDataProductoCuota();

        await GetDataOficina();
        await GetDataTasa();

        codigoVendedor = cuotaVentas.Vendedor;
        if (Accion == "Edit")
        {
            IsDisabled = true;
        }
        else
        {
            IsDisabled = false;
        }

    }


    async Task GetDataOficina()
    {
        oficinaList = await oficinaService.GetOficinaAsync();
    }
    async Task OficinaSelectionChanged(ChangeEventArgs e)
    {

        if (int.TryParse(e.Value.ToString(), out int id))
        {
            oficinaId = id;

            vendedorList = await vendedorService.GetVendedorAsync(id);
        }

    }

    void ProductoSelectionChanged(ChangeEventArgs e)
    {

        if (int.TryParse(e.Value.ToString(), out int id))
        {
            idProductoCuota = id;

            cuotaVentas.IdProductoCuota = id;
        }

    }


    void VendedorSelectionChanged(ChangeEventArgs e)
    {
        string id = e.Value.ToString();

        codigoVendedor = id;
        cuotaVentas.Vendedor = codigoVendedor;

    }
    void TasaSelectionChanged(ChangeEventArgs e)
    {
        if (int.TryParse(e.Value.ToString(), out int id))
        {

            tasaSelected = pCTasaAñoMes.Where(t => t.Id == id).FirstOrDefault();
            cuotaVentas.Año = tasaSelected.Año;
            cuotaVentas.Mes = tasaSelected.Mes;


        }






    }




    async Task GetDataProductoCuota()
    {
        productoCuota = await productoCuotaService.getProductoCuota();
    }
    async Task GetDataTasa()
    {
        pCTasaAñoMes = await tasaAñoMesService.getTasaAñoMesnAsync();

    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private TasaAñoMesService tasaAñoMesService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ProductoCuotaService productoCuotaService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private VendedorService vendedorService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private OficinaService oficinaService { get; set; }
    }
}
namespace __Blazor.Compensation.Client.Pages.CuotaVentas.FormularioCuotaVentas
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateInputNumber_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Class", __arg0);
        __builder.AddAttribute(__seq1, "disabled", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateInputNumber_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, System.Object __arg1, int __seq2, TValue __arg2, int __seq3, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg3, int __seq4, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg4)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Class", __arg0);
        __builder.AddAttribute(__seq1, "disabled", __arg1);
        __builder.AddAttribute(__seq2, "Value", __arg2);
        __builder.AddAttribute(__seq3, "ValueChanged", __arg3);
        __builder.AddAttribute(__seq4, "ValueExpression", __arg4);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_3<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_4<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateInputNumber_5<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, System.Object __arg0, int __seq1, TValue __arg1, int __seq2, global::Microsoft.AspNetCore.Components.EventCallback<TValue> __arg2, int __seq3, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg3)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.InputNumber<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Class", __arg0);
        __builder.AddAttribute(__seq1, "Value", __arg1);
        __builder.AddAttribute(__seq2, "ValueChanged", __arg2);
        __builder.AddAttribute(__seq3, "ValueExpression", __arg3);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_6<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
