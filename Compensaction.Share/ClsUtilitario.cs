﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Compensaction.Share
{
    public class ClsUtilitario
    {


        public static string ConvertSha1(string str)
        {
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1Managed.Create();
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] stream = null;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i <= (stream.Length - 1); i++)
            {
                sb.AppendFormat("{0:x2}", stream[i]);
            }
            return sb.ToString();
        }


    }
}
