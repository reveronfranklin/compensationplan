#pragma checksum "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Shared\MenuMaestros.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6a87e3e9646f90a52f7bcdd7c03e90ade302dc24"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace Compensation.Client.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#line 1 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#line 2 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#line 3 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#line 4 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#line 5 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#line 6 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#line 7 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client;

#line default
#line hidden
#line 8 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Shared;

#line default
#line hidden
#line 9 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensaction.Share;

#line default
#line hidden
#line 10 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using Compensation.Client.Helpers;

#line default
#line hidden
#line 11 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\_Imports.razor"
using BlazorContextMenu;

#line default
#line hidden
    public partial class MenuMaestros : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#line 15 "C:\Projects\ClientSide\CompensationPlan\CompensationPlan\Compensation.Client\Shared\MenuMaestros.razor"
      


    async Task MenuSelectionChanged(ChangeEventArgs e)
    {
        string id = e.Value.ToString();
        if (id == "1")
        {
            UriHelper.NavigateTo("indice-periodo");
        }
          if (id == "2")
        {
            UriHelper.NavigateTo("indice-FlatComision");
        }
        if (id == "3")
        {
            UriHelper.NavigateTo("indice-FlatComisionGerente");
        }
         if (id == "4")
        {
            UriHelper.NavigateTo("indice-RangoCumplimientoCuotaGeneral");
        }
        if (id == "5")
        {
            UriHelper.NavigateTo("indice-PorcCantidadCategoriasCubiertas");
        }
        if (id == "6")
        {
            UriHelper.NavigateTo("indice-tasa-año-mes");
        }
        if (id == "7")
        {
            UriHelper.NavigateTo("indice-ProductoCuota");
        }
         if (id == "8")
        {
            UriHelper.NavigateTo("indice-CuotaVentas");
        }
          if (id == "9")
        {
            UriHelper.NavigateTo("indice-OrdenesPignoradas");
        }
        
    }

#line default
#line hidden
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager UriHelper { get; set; }
    }
}
#pragma warning restore 1591
